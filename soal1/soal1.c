#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <stdint.h>
#include <dirent.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/wait.h>

/* UTILITY FUNCTIONS */

// decode base64 -> binary functions
int b64_decodeTable[] = { 62, -1, -1, -1, 63, 52, 53, 54, 55, 56, 57, 58,
	59, 60, 61, -1, -1, -1, -1, -1, -1, -1, 0, 1, 2, 3, 4, 5,
	6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
	21, 22, 23, 24, 25, -1, -1, -1, -1, -1, -1, 26, 27, 28,
	29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42,
	43, 44, 45, 46, 47, 48, 49, 50, 51 };

// check decoded size
size_t b64_decodedSize(const char *inputString)
{
	size_t inputLength;
	size_t outputLength;

    // iterator
	size_t i;

	if (inputString == NULL)
		return 0;

	inputLength = strlen(inputString);
	outputLength = inputLength / 4 * 3;

    i = inputLength;
	while(i > 0) {
		if (inputString[i] == '=') {
			outputLength--;
		} else {
			break;
		}
        i--;
	}

	return outputLength;
}

// check if base64 chars is valid (not newline, etc)
int b64_isValidChar(char c) {
    if (c >= '0' && c <= '9') return 1;
	if (c >= 'A' && c <= 'Z') return 1;
	if (c >= 'a' && c <= 'z') return 1;
	if (c == '+' || c == '/' || c == '=') return 1;

	return 0;
}

// decode base4
int b64_decode(const char *inputString, unsigned char *decodedString, size_t outLength) {
    size_t inputLength;
    int decodeHandler;

    // iterators (i for b64, j for binary)
    size_t i, j;

    // if input == null nothing to decode
    if(inputString == NULL || decodedString == NULL) return 0;

    // check if the base64 string is valid to decode
    inputLength = strlen(inputString);
    if(outLength < b64_decodedSize(inputString) || inputLength % 4 != 0) return 0;

    // check char validation
    for(i = 0; i<inputLength; i++) {
        if(!b64_isValidChar(inputString[i])) return 0;
    }

    // traverse to decode
    for(i = 0, j = 0; i < inputLength; i+=4, j+=3) {
        decodeHandler = b64_decodeTable[inputString[i]-43];
        decodeHandler = (decodeHandler << 6) | b64_decodeTable[inputString[i+1]-43];

        // if padding found
        if(inputString[i+2] == '=') decodeHandler = decodeHandler << 6;
        else decodeHandler = (decodeHandler << 6) | b64_decodeTable[inputString[i+2]-43];

        if(inputString[i+3] == '=') decodeHandler = decodeHandler << 6;
        else decodeHandler = (decodeHandler << 6) | b64_decodeTable[inputString[i+3]-43];

        // base16
        decodedString[j] = (decodeHandler >> 16) & 0xff;

        // find padding
        if(inputString[i+2] != '=') decodedString[j+1] = (decodeHandler >> 8) & 0xff;
        if(inputString[i+3] != '=') decodedString[j+2] = decodeHandler & 0xff;
    }

    return 1;
}

// end decode base64 -> binary functions

/* ===== END UTILITY FUNCTIONS ===== */

/* ===== UTILITY VARIABLES ===== */

pthread_t tid[15];
char modul3Path[200] = "/home/anggito/Documents/sisop/praktikum/res/modul-3";

/* ===== END UTILITY VARIABLES ===== */

void *mkdir_modul3(void *arg) {
    // printf("Hello0\n");
    if(fork() == 0) execl("/bin/mkdir", "mkdir_modul3", "-p", modul3Path, NULL);

    return NULL;
}

void *unzip_quote(void *arg) {
    // printf("Hello1\n");
    if(fork() == 0) execl("/bin/unzip", "unzip_quote", "-q", "/home/anggito/Downloads/quote.zip", "-d", modul3Path, NULL);

    return NULL;
}

void *unzip_music(void *arg) {
    // printf("Hello2\n");
    if(fork() == 0) execl("/bin/unzip", "unzip_music", "-q", "/home/anggito/Downloads/music.zip", "-d", modul3Path, NULL);

    return NULL;
}

void *mkdir_hasil(void *arg) {
    // printf("Hello3\n");
    if(fork() == 0) execl("/bin/mkdir", "mkdir_hasil" , "-p", strcat(modul3Path, "/hasil"), NULL);
}

void *touch_quote(void *arg) {
    // printf("Hello4\n");
    if(fork() == 0) execl("/bin/touch", "touch_quote", strcat(modul3Path, "/hasil/quote.txt"), NULL);
}

void *touch_music(void *arg) {
    // printf("Hello5\n");
    if(fork() == 0) execl("/bin/touch", "touch_music", strcat(modul3Path, "/hasil/music.txt"), NULL);
}

void *decode_b64(void *arg) {
    // printf("Hello6\n");
    if(chdir(modul3Path) < 0) perror("THREAD 5: Error chdir modul3\n");

    // Listing variables
    DIR *dir_path;
    struct dirent *curr_file;

    FILE *quote, *music, *reader;

    dir_path = opendir(".");

    quote = fopen("./hasil/quote.txt", "w+");
    music = fopen("./hasil/music.txt", "w+");

    // Listing for q and m
    if(dir_path != NULL) {
        while(curr_file = readdir(dir_path)) {
            char rawContent[256];

            // Decode variables
            char b64Input[256];
            char *b64Output;
            size_t outLength;

            if(strstr(curr_file->d_name, "q") || strstr(curr_file->d_name, "m")) {
                // read content
                // printf("%s\n", curr_file->d_name);
                char cwd[200];
                getcwd(cwd, sizeof(cwd));

                char pathFileToRead[256] = "";
                strcat(pathFileToRead, cwd);
                strcat(pathFileToRead, "/");
                strcat(pathFileToRead, curr_file->d_name);

                reader = fopen(pathFileToRead, "r");

                if(fgets(rawContent, 256, reader) != NULL) {
                    // Decode data
                    strcpy(b64Input, rawContent);
                }

                // allocate memory for output string
                outLength = b64_decodedSize(b64Input) + 1;
                b64Output = malloc(outLength);

                // decode the b64 data
                if(!b64_decode(b64Input, (unsigned char *) b64Output, outLength)) {
                    printf("THREAD 5: Decode Failure\n");
                    return NULL;
                }
                b64Output[outLength] = '\0';

                // store to .txt
                if(strstr(curr_file->d_name, "q")) {
                    // printf("b64out - q : %s\n", b64Output);
                    if(fprintf(quote, "%s\n", b64Output) < 0) perror("THREAD 5: Error store to quote.txt");
                }
                else if(strstr(curr_file->d_name, "m")) {
                    // printf("b64out - m : %s\n", b64Output);
                    if(fprintf(music, "%s\n", b64Output) < 0) perror("THREAD 5: Error store to music.txt");
                }
            }
        }
        fclose(reader);
        fclose(quote);
        fclose(music);
    }
    closedir(dir_path);
}

void *zip_hasil(void *arg) {
    // printf("Hello7\n");
    if(chdir(modul3Path) < 0) perror("THREAD 5: Error chdir modul3\n");
    
    if(fork() == 0) {
        char zipPath[256] = "";
        strcat(zipPath, modul3Path);
        strcat(zipPath, "/hasil");
        execl("/bin/zip", "zip_hasil", "-q", "-P", "mihinomenestanggito", "hasil.zip", zipPath, NULL);
    }
}

void *write_no(void *arg) {
    // printf("Hello9\n");

    char noPath[256] = "";
    strcat(noPath, modul3Path);
    strcat(noPath, "/no.txt");

    FILE *no;
    no = fopen(noPath, "w+");
    if(fprintf(no, "No") < 0) perror("Error Write No");
    fclose(no);
}

void *zip_newHasil(void *arg) {
    // printf("Hello10\n");
    if(chdir(modul3Path) < 0) perror("THREAD 5: Error chdir modul3\n");
    
    if(fork() == 0) {
        char zipPath[256] = "", noPath[256] = "";

        strcat(zipPath, modul3Path);
        strcat(zipPath, "/hasil");

        strcat(noPath, modul3Path);
        strcat(noPath, "/no.txt");

        execl("/bin/zip", "zip_hasil", "-q", "-r", "-P", "mihinomenestanggito", "hasil.zip", zipPath, noPath , NULL);
    }
}

int main() {
    int err;

    // mkdir_modul3
    pthread_create(&(tid[0]), NULL, &mkdir_modul3, NULL);
    pthread_join(tid[0], NULL);

    // unzip_quote -- unzip_music
    pthread_create(&(tid[1]), NULL, &unzip_quote, NULL);
    pthread_create(&(tid[2]), NULL, &unzip_music, NULL);
    pthread_join(tid[1], NULL);
    pthread_join(tid[2], NULL);

    // mkdir_hasil
    pthread_create(&(tid[3]), NULL, &mkdir_hasil, NULL);
    pthread_join(tid[3], NULL);

    // touch quote.txt -- touch music.txt
    pthread_create(&(tid[4]), NULL, &touch_quote, NULL);
    pthread_create(&(tid[5]), NULL, &touch_music, NULL);
    pthread_join(tid[4], NULL);
    pthread_join(tid[5], NULL);

    // decode b64
    pthread_create(&(tid[6]), NULL, &decode_b64, NULL);
    pthread_join(tid[6], NULL);

    // zip hasil
    pthread_create(&(tid[7]), NULL, &zip_hasil, NULL);
    pthread_join(tid[7], NULL);

    // write no.txt
    pthread_create(&(tid[8]), NULL, &write_no, NULL);
    pthread_join(tid[8], NULL);

    // zip new hasil
    pthread_create(&(tid[9]), NULL, &zip_newHasil, NULL);
    pthread_join(tid[9], NULL);

    exit(0);
    return 0;
}