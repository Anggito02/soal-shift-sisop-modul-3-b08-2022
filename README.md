# soal-shift-sisop-modul-3-B08-2022

## Daftar Isi

- [Anggota Kelompok](#anggota-kelompok)
- [Nomor 1](#nomor-1)
    - Deskripsi Soal
    - [Kendala Nomor 1](#kendala-nomor-1)
    - [Solusi Nomor 1](#solusi-nomor-1)
    - [Source Code Nomor 1](https://gitlab.com/Anggito02/soal-shift-sisop-modul-3-b08-2022/-/tree/main/soal1)
- [Nomor 2](#nomor-2)
    - Deskripsi Soal
    - [Kendala Nomor 2](#kendala-nomor-2)
    - [Solusi Nomor 2](#solusi-nomor-2)
    - [Source Code Nomor 2](https://gitlab.com/Anggito02/soal-shift-sisop-modul-3-b08-2022/-/tree/main/soal2)
- [Nomor 3](#nomor-3)
    - Deskripsi Soal
    - [Kendala Nomor 3](#kendala-nomor-3)
    - [Solusi Nomor 3](#solusi-nomor-3)
    - [Source Code Nomor 3](https://gitlab.com/Anggito02/soal-shift-sisop-modul-3-b08-2022/-/tree/main/soal3)
    
## Anggota Kelompok

- Anggito Anju Hartawan Manalu	        5025201216
- Muhammad Yusuf Singgih Maulana	    5025201146
- Izzati Mukhammad	                    5025201075

## Nomor 1

### Deskripsi Soal

Pada problem ini kita diberikan 2 folder zip yang berisi file-file yang dituliskan dalam format base64. Kita diminta untuk men-decode semua file-file tersebut menjadi format binary. Pertama-tama
 
(1.a) Kita harus meng-unzip dua folder zip, yakni music.zip dan quote.zip. Unzip harus dilakukan menggunakan thread.

(1.b) Setelah itu, kita harus men-decode semua file dengan format .txt dan memasukkan filenya masing-masing pada file music.txt dan quote.txt. Setiap kalimat dipisahkan oleh newline/enter.

(1.c) Kemudian pindahkan kedua file .txt tersebut ke folder bernama hasil.

(1.d) Kemudian kita harus meng-zip folder hasil menjadi hasil.zip. Kita harus memberikan password pada file zip tersebut dengan password 'mihinomenest"nama-user"'.

(1.e) Karena ada yang kurang, kita harus meng-unzip hasil.zip dan menambahkan file bernama no.txt yang berisi "No" yang kemudian di zip kembali dengan folder hasil tersebut dengan password yang sama seperti sebelumnya.

### Kendala Nomor 1

Kadang terjadi error saat program dijalankan. Tidak ada penyelesaian terhadap kendala tersebut. Kami sudah mencoba mencari dari berbagai sumber dan kemungkinan terjadi kesalahan pada cache memori dari linux sendiri. Sampai saat ini kami masih belum bisa menemukan solusi dari kendala tersebut.

### Solusi Nomor 1

#### Penjelasan singkat dalam penyelesaian nomor 1

Dalam penggunaan multi-threading di satu proses, akan dibutuhkan fungsi untuk mewakili satu demi satu thread yang dijalankan. Beberapa thread, seperti soal 1.a mengharuskan untuk dijalankan dalam waktu yang bersamaan. Hal ini akan membedakan penggunaan fungsi `pthread_join()`, yakni fungsi wait yang ada pada thread.

#### 1.a Unzip music.txt dan quote.txt

Kita akan menggunakan 2 thread untuk menyelesaikan sub-soal ini. Thread pertama digunakan untuk unzip music.txt dan thread kedua digunakan untuk unzip quote.txt. Kedua thread ini akan di create bersamaan baru di-joinkan pada main-process agar proses kedua thread berjalan bersamaan. Berikut implementasinya

<b>CREATE dan JOIN thread pada fungsi main</b>
<br>
<br>
<img src="/img/1/gambar-1-a-1.png" alt="create-join-1-2-main">

<b>Fungsi  unzip_quote dan unzip_music</b>
<br>
<br>
<img src="/img/1/gambar-1-a-2.png" alt="unzips-functions">

#### 1.c 1.b Membuat quote.txt dan music.txt pada folder hasil dan men-decode base64

Sebagai persiapan sebelum men-decode base64. Kita akan membuat quote.txt dan music.txt yang nantinya akan menjadi list dari base64 yang telah di-decode. Kemudian, kita akan melakukan listing direktori yang berisi file-file yang berasal dari hasil extract quote.zip dan music.zip. Setelah itu kita baru listing untuk men-decodenya satu per satu.

Pertama-tama kita harus membuat fungsi utilitas yang nantinya akan digunakan untuk men-decode base64. 
Untuk utilitas pertama adalah decode table yang digunakan untuk mendecode base64 ke binary.
Untuk utilitas kedua adalah decode size yang berfungsi untuk menentukan ukuran dari binary yang menjadi hasil decode dari base64.
Utilitas ketiga adalah untuk menentukan apakah base64 memiliki karakter yang valid atau tidak.

Kemudian, kita akan membuat fungsi utama untuk mendecode b64 menjadi binary. Fungsi ini akan membutuhkan beberapa parameter input, yakni: 1. Pointer dari string yang masih dalam format base64, 2. Pointer dari string yang akan menjadi hasil decode, dan 3. Panjang/ukuran dari string hasil decode yang didapat dari utiliitas kedua.

Setelah semua .txt sudah di decode dan dimasukkan pada .txt tujuan yang sesuai, kita akan close semua stream yang digunakan. Berikut implementasinya.

<b>CREATE dan JOIN thread hasil, quote.txt, dan music.txt pada fungsi main</b>
<br>
<br>
<img src="/img/1/gambar-1-c-1.png" alt="create-join-c">

<b>MKDIR hasil, TOUCH music.txt dan quote.txt</b>
<br>
<br>
<img src="/img/1/gambar-1-c-2.png" alt="mkdir-touch-c">

<b>CREATE dan JOIN thread decode b64 pada fungsi main</b>
<br>
<br>
<img src="/img/1/gambar-1-b-1.png" alt="create-join-decode">

<b>THREAD untuk melakukan decode pada tiap file .txt</b>
<br>
<br>
<img src="/img/1/gambar-1-b-2.png" alt="decode_b64">

<b>Utilitas 1 - Decode Table</b>
<br>
<br>
<img src="/img/1/gambar-1-b-3.png" alt="decode_table">

<b>Utilitas 2 - Decode Size</b>
<br>
<br>
<img src="/img/1/gambar-1-b-4.png" alt="decode_size">

<b>Utilitas 3 - Char Validator</b>
<br>
<br>
<img src="/img/1/gambar-1-b-5.png" alt="decode_valid">

#### 1.d Zip folder hasil dan memberikan password

Untuk melakukan zip pada sebuah folder, kita dapat menggunakan fungsi `exec()` dan menggunakan fungsi bash `zip`. Untuk memberikan password pada zip tersebut, kita dapat menggunakan `-P` diikuti dengan password yang kita inginkan (pada kasus ini passwordnya: mihinomenestanggito). Berikut implementasinya.

<b>CREATE dan JOIN thread zip hasil pada fungsi main</b>
<br>
<br>
<img src="/img/1/gambar-1-d-1.png" alt="zip_main">

<b>ZIP hasil</b>
<br>
<br>
<img src="/img/1/gambar-1-d-2.png" alt="zip_hasil">

#### 1.e Menambahkan file no yang kemudian di zip bersama hasil

Pertama-tama kita harus membuat `no.txt` dan menuliskan konten "No". Setelah itu, kita akan meng-zipnya bersamaan dengan folder hasil dengan menambahkan `-r` karena hasil merupakan folder, sedangkan no.txt merupakan file. Berikut implementasinya.

<b>CREATE dan JOIN thread no.txt, zip hasil pada fungsi main</b>
<br>
<br>
<img src="/img/1/gambar-1-e-1.png" alt="zip_no_main">

<b>Menulis no.txt dan meng-zipnya bersamaan dengan folder hasil</b>
<br>
<br>
<img src="/img/1/gambar-1-e-2.png" alt="touch-no-zip-hasil">

## Nomor 2

### Deskripsi Soal

Soal nomor 2, kita harus membuat sistem online judge sederhana. Pada sistem ini kita diharuskan untuk meng-implementasikan Socket Programming untuk meng-handle Client dan Server. Pada soal ini server berfungsi sebagai penyimpan database dan client diberikan beberapa akses sesuai yang tertulis pada sub-soal.

(2.a) Client harus dapat melakukan register dan login pada sistem.

Untuk alur register, client dapat membuat akun baru dengan mencantumkan username. Username ini harus bersifat baru yang unik dan tidak terdapat pada database sistem sebelumnya. Setelah username dinyatakan valid oleh server, client dapat mencantumkan password baru yang diinginkan untuk akun tersebut. Setelah client memberikan informasi tersebut, server harus menyimpannya dalam <b>users.txt</b> dengan format <b>username:password</b> dan setiap akun akan dipisahkan oleh <b>newline</b>.

Untuk alur login, client diharuskan untuk mencantumkan username dan password dari akun yang sudah pernah terdaftar dan tersimpan dalam database server. Jika username dan password yang dicantumkan tidak terdapat pada database server, yakni <b>users.txt</b>, maka server akan menolak permintaan login client.

(2.g) Server harus dapat meng-handle lebih dari satu client. Meskipun begitu, server hanya dapat melayani satu client dalam satu waktu dan menahan client lainnya dalam posisi SUSPEND.

(2.b) Saat sistem dijalankan, database harus membuat database soal bernama <b>problems.tsv</b> jika masih belum terdapat pada server sebelumnya. Isi database ini adalah judul problem dan author problem yang membuatnya dengan format <b>judul-problem\tauthorproblem</b>.

Client akan memiliki dua state, yaknik LOGIN_STATE dan LOGOUT_STATE. Pada login state, client dapat memasukkan beberapa command, yaitu:

(2.c) 1. "add". Command ini digunakan agar client dapat memasukkan soal baru pada database server. Command ini akan di-pass pada server. Jika server menerima command ini, server akan meminta 4 kebutuhan khusus untuk soal baru, yakni: 1. Judul problem, 2. Filepath ke deskripsi dari soal tersebut, 3. Filepath ke input dari soal tersebut, dan 4. Filepath ke output dari soal tersebut. Untuk judul dari problem terdapat syarat bahwa judul soal yang dicantumkan harus unik dan tidak ada di database server sebelumnya.
Setelah client mencantumkan keempat data tersebut, server harus mengeluarkan output dengan format

    Judul problem:
    Filepath description.txt:
    Filepath input.txt:
    Filepath output.txt:

Kemudian server akan menyimpan data judul problem beserta username akun yang mencantumkannya pada <b>problems.tsv</b> dengan format yang sudah diberikan diatas. Selain itu, server akan menyimpan soal baru tersebut pada folder yang memiliki nama sesuai dengan judul problem yang sudah diberikan oleh client. Didalamnya terdapat description.txt, input.txt, dan output.txt yang juga sudah dicantumkan oleh client.

(2.d) 2. "see". Command ini digunakan agar client dapa melihat problems apa saja yang tersedia pada server. Saat server menerima command ini, server akan menampilkan semua data problems dengan format <b>judul-problem by author</b> dan dipisahkan oleh newline.

(2.e) 3. "download 'judul-problem'". Command ini digunakan agar client dapat men-download problem dengan judul yang telah dicantumkan. Setelah server mendapatkan command ini, server akan membuat folder pada client dengan nama judul-problem yang di-download. Folder tersebut akan berisi description.txt dan input.txt dari problem yang di-download oleh client.

(2.f) 4. "submit 'judul-prolem' 'filepath output dari client'". Command ini digunakan agar client dapat memberikan output/ jawaban dari problem dengan judul-problem yang ingin di solve oleh client pada saat meng-submit. Saat server mendapatkan command ini, server akan mengecek isi dari output/jawaban dari client dan mengecek apakah jawaban tersebut benar atau salah. Jika jawaban yang disampaikan benar, server akan mengirimkan "AC" pada client dan mengirimkan "WA" jika jawaban salah.

### Kendala Nomor 2

- Terdapat sedikit kendala jika memeberikan koneksi pada terlalu banyak client.

- Penjelasan: Pada program ini, kami membuat 15 thread untuk meng-handle multi-connection terhadap client yang terkoneksi. Karena thread yang diatur tidak bersifat dinamis sesuai client yang terkoneksi melainkan terbatas/ statis (pada kasus ini maksimal 15 client terkoneksi secara bersamaan), maka akan terjadi error (SEGMENTATION FAULT) pada server jika ada client ke-16 mencoba membuat koneksi dengan server.

### Solusi Nomor 2

#### Penjelasan Singkat mengenai Socket Programming

Karena nomor 2 membutuhkan memiliki 2 program yang saling terkoneksi satu sama lain, maka kita akan menggunakan Socket Programming untuk membuat koneksi 2 program tersebut. Untuk pengaturan koneksi awal, kita dapat mencocokkan port dan IP, sehingga server dan client dapat terkoneksi. Berikut implementasinya.

<b>SERVER</b>
<br>
<br>
<img src="/img/2/gambar-2-0-1.png" alt="server-socket">

<b>CLIENT</b>
<br>
<br>
<img src="/img/2/gambar-2-0-2.png" alt="client-socket">

#### 2.a Login dan Register pada Client

Karena server dan client sudah terhubung, kita diminta untuk memberikan beberapa command yang dapat dicantumkan oleh client pada saat masih dalam posisi LOGOUT_STATE, yakni register dan login.

Untuk register, kita harus memasukkan data username dan password client yang kemudian diterima oleh server dan disimpan pada database. Akan tetapi, ada beberapa syarat. Username harus bersifat unik dan password minimal terdiri dari 6 huruf, terdapat angka, terdapat huruf besar dan kecil. Setelah data akun baru diterima oleh server. Data tersebut akan dituliskan pada <b>user.txt</b>. Berikut implementasinya.

<b>CLIENT mengirimkan command register dan informasi username</b>
<br>
<br>
<img src="/img/2/gambar-2-a-1.png" alt="client-pass-register">

<b>SERVER menerima command register dan informasi username kemudian menentukan apakah username valid atau tidak.</b>
<br>
<br>
<img src="/img/2/gambar-2-a-2.png" alt="server-receive-register-pass-unameValid">

<b>CLIENT memberikan informasi password, jika username VALID</b>
<br>
<br>
<img src="/img/2/gambar-2-a-3.png" alt="client-pass-password">

<b>SERVER menerima password dan mengecek apakah valid.</b>
<br>
<br>
<img src="/img/2/gambar-2-a-4.png" alt="server-check-passwordVAlid">

<b>CLIENT diubah statenya menjadi LOGIN</b>
<br>
<br>
<img src="/img/2/gambar-2-a-5.png" alt="client-change-state">

<b>TESTING RUN</b>
<br>
<br>
<img src="/img/2/gambar-2-a-6.png" alt="test-run-2a-login">

<b>HASIL USERS.TXT</b>
<br>
<br>
<img src="/img/2/gambar-2-a-7.png" alt="hasil-test-users.txt">

Untuk login, client dapat mencantumkan username dan password yang telah terdaftar pada database users.txt yang terdapat pada server. Berikut implementasinya.

<b>CLIENT mengirim command login dan informasi akun</b>
<br>
<br>
<img src="/img/2/gambar-2-a-8.png" alt="client-send-login">

<b>SERVER cek validasi akun, dan send hasil validasi</b>
<br>
<br>
<img src="/img/2/gambar-2-a-9.png" alt="server-check-login">

<b>TESTING RUN</b>
<br>
<br>
<img src="/img/2/gambar-2-a-10.png" alt="test-run-2a-Register">

#### 2.b Server membuat problems.tsv

Server diminta untuk membuat <b>problems.tsv</b> jika belum tersedia sebelumnya. Berikut implementasinya.

<b>SERVER menggunakan fopen untuk membuat problems.tsv</b>
<br>
<br>
<img src="/img/2/gambar-2-b-1.png" alt="server-fopen-problems.tsv">

<b>TESTING RUN</b>
<br>
<br>
<img src="/img/2/gambar-2-b-2.png" alt="test-run-2b">

#### 2.c Command add pada client

Client yang telah login diharapkan untuk mendapat 4 command. Command yang pertama adalah "add" dimana client dapat memasukkan problem baru pada database server. Problem baru akan disimpan pada problems.tsv dengan format 'judul-problems/tauthor'. Setelah itu, akan dibuat folder dengan nama judul problem baru yang berisi deskripsi, input, dan output dari problem baru tersebut. Berikut implementasinya.

<b>CLIENT memberikan command "add" dan detail problems</b>
<br>
<br>
<img src="/img/2/gambar-2-c-1.png" alt="client-pass-add">

<b>SERVER menyimpan detail problem dan menge-print detail pada server-side</b>
<br>
<br>
<img src="/img/2/gambar-2-c-2.png" alt="server-receive-n-print-detail">

<b>TESTING RUN</b>
<br>
<br>
<img src="/img/2/gambar-2-c-3.png" alt="test-run-2c">

<b>HASIL PROBLEMS.TSV</b>
<br>
<br>
<img src="/img/2/gambar-2-c-4.png" alt="hasil-test-problems.tsv">

<b>HASIL FOLDER PADA SERVER</b>
<br>
<br>
<img src="/img/2/gambar-2-c-5.png" alt="hasil-problem-folder">

<b>HASIL DETAIL SOAL PADA SERVER</b>
<br>
<br>
<img src="/img/2/gambar-2-c-6.png" alt="hasil-detail-problem">

#### 2.d Command see pada client

Command kedua adalah see, dimana client dapat melihat list dari problems yang tersedia di database problems yang berada server. Server dapat menampilkan apa yang tertulis di <b>problems.tsv</b> dan mengembalikan informasinya dalam format yang tepat pada client. Berikut implementasinya.

<b>CLIENT mengirimkan command see pada server</b>
<br>
<br>
<img src="/img/2/gambar-2-d-1.png" alt="client-send-see">

<b>SERVER menampilkan list problems</b>
<br>
<br>
<img src="/img/2/gambar-2-d-2.png" alt="server-send-list">

<b>TESTING RUN</b>
<br>
<br>
<img src="/img/2/gambar-2-d-3.png" alt="test-run-2d">

#### 2.e Command download dari client

Client dapat melakukan download soal dengan judul tertentu dengan memasukkan command "download 'judul-problem'". Dengan memasukkan command tersebut, server akan mengirimkan informasi deskripsi dan input dari problem tersebut. Selain itu, server akan membuat folder tempat menyimpan deskripsi dari input problem tersebut pada tempat penyimpanan client. Berikut Implementasinya.

<b>CLIENT mengirim command download dan judul problem</b>
<br>
<br>
<img src="/img/2/gambar-2-e-1.png" alt="client-send-download-n-title">

<b>SERVER menerima command dan mebuat folder yang berisi informasi soal yang di-download</b>
<br>
<br>
<img src="/img/2/gambar-2-e-2.png" alt="server-send-folders">

<b>TESTING RUN</b>
<br>
<br>
<img src="/img/2/gambar-2-e-3.png" alt="test-run-2e">

<b>CLIENT folder yang telah di-download</b>
<br>
<br>
<img src="/img/2/gambar-2-e-4.png" alt="client-downloaded-folder">

#### 2.f Command submit dari client

Client dapat melakukan submit pada problem dengan judul tertentu yang kemudian akan di cek oleh server apakah jawaban yang diberikan benar atau salah. Format command submit dari client adalah "submit 'judul-problem' 'filepath output dari client'".

<b>CLIENT mengirimkan command submit, judul problem, dan filepath dari output</b>
<br>
<br>
<img src="/img/2/gambar-2-f-1.png" alt="client-submit">

<b>SERVER mengecek jawaban dari client</b>
<br>
<br>
<img src="/img/2/gambar-2-f-2.png" alt="server-check">

<b>CLIENT mendapat feedback dari server</b>
<br>
<br>
<img src="/img/2/gambar-2-f-3.png" alt="client-get-feedback">

<b>TESTING RUN</b>
<br>
<br>
<img src="/img/2/gambar-2-f-4.png" alt="test-run-2f">

#### 2.g Multiple Connections

Server dapat menampung lebih dari 1 koneksi client dengan menggunakan thread. Thread/client lain akan menunggu hingga thread pertama sudah selesai diproses oleh server.

<b>SERVER SIDE MULTIPLE CONNECTIONS</b>
<br>
<br>
<img src="/img/2/gambar-2-g-1.png" alt="server-multiconnection">

<b>CLIENT SIDE MULTIPLE CONNECTIONS</b>
<br>
<br>
<img src="/img/2/gambar-2-g-2.png" alt="client-multiconnection">

<b>TESTING RUN</b>
<br>
<br>
<img src="/img/2/gambar-2-g-3.png" alt="test-run-2g">

## Nomor 3

### Deskripsi Soal

Program dijalankan untuk mengategorikan file-file secara rekursif berdasarkan jenis ekstensinya dengan cara memindahkan file-file tersebut ke dalam directori bernama jenis ekstensinya masing masing. Perlu diketahui bahwa nama direktori ekstensinya semuanya lowercase, agar dapat mengakomodasi ekstensi yang tidak case sensitive. Mengategorikan semua file di dalam working directory semua directori ekstensi terletak di directory yang sama dengan file yang dijalankan.

Jika file tidak memiliki ekstensi, maka filenya diletakan di direktori "Unknown". Jika file dalam mode hidden, maka filenya diletakan di direktori "Hidden". Setiap file yang dipindahkan memiliki satu thread agar prosesnya bisa berjalan secara bersamaan. 

Kemudian program dijalankan dengan menggunakan client server dan pada saat client dijalankan maka directori hartakarun akan di zip dengan nama hartakarun.zip ke working directori client dan client dapat mengirimkan file hartakarun.zip tersebut ke server dengan mengirimkan command ke server "send hartakarun.zip"

### Kendala Nomor 3

- Kendala pada nomor ini adalah nama file yang memiliki "." lebih dari satu jadi sulit untuk mendapatkan ekstensi yang aslinya
- Hidden file masih belum meremove char pada awal nama 
- Segmentation fault pada saat strcat

### Solusi Nomor 3

- Penyelesaian dari permasalahan diatas adalah dengan menganggap nama file yang memiliki "." lebih dari satu sebagai ekstensi itu sendiri dan dengan mengganti awal nama pada hidden file dengan "-"

#### 3.a, b

Terdapat dua fungsi pada file soal3.c ini yaitu handler dan listFilesRecursively. Untuk working directorinya dibuat dengan menggunakan program yaitu dengan menggunakan `fork()` seperti pada modul sebelumnya. Kemudian berikut merupakan penjelasan dari fungsi fungsi yang ada pada file soal3.c

    void* handler(void *arg){
    
        char things[200];
        strcpy(things,arg);

        unsigned long i=0;
        pthread_t id=pthread_self();
        int iter, index=0;
        char *arr2[50];
        char namaFile[200];
        char argCopy[100];

        //ngambil namafile beserta eksistensi
        char *token1 = strtok(things, "/");
        while (token1 != NULL) {
                arr2[index++] = token1;
                token1 = strtok(NULL, "/");
        }
        strcpy(namaFile,arr2[index-1]);

        //cek filenya termasuk dalam folder apa
        char *token = strchr(namaFile, '.');
        if (token == NULL) {
            strcat(argCopy, "Unknown");
        }
        else if (namaFile[0] == '.') {
            strcat(argCopy, "Hidden");
        }
        else {
            strcpy(argCopy, token+1);
            for (int i = 0; argCopy[i]; i++) {
                argCopy[i] = tolower(argCopy[i]);
            }
        }

        char source[1000], target[1000], dirToBeCreate[120];
        char *unhide, unhidden[200];
        strcpy(source, arg);
        if (sinyal == 1) {
            sprintf(target, "/home/nauli/shift3/hartakarun/%s/%s", argCopy, namaFile);
            sprintf(dirToBeCreate, "/home/nauli/shift3/hartakarun/%s/", argCopy);
            mkdir(dirToBeCreate, 0750);   
        } else if (sinyal == 2 || sinyal == 3) {
            if (namaFile[0] == '.') {
                namaFile[0] = '-';
            }
            sprintf(target, "%s/%s", argCopy, namaFile);
            sprintf(dirToBeCreate, "%s/", argCopy);
            mkdir(dirToBeCreate, 0750);
        }

        //pindah file
        if (rename(source,target) == 0) {
            printf("Berhasil Dikategorikan\n");
        } else printf("Gagal dikategorikan :(\n");

        return NULL;
    }

- Fungsi tersebut akan mengecek apakah file tersebut memiliki ekstensi, tidak memiliki ekstensi, atau hidden (Yang berawalan titik). Lalu file tersebut dipindahkan ke folder yang sesuai dengan ekstensinya.
- Setelah mengkategorikan folder tujuan, directory ditentukan berdasarkan sinyal. Pada variable sinyal, bila bernilai 1 maka directory tujuan ditentukan pada "/home/[user]/modul3/hartakarun/". Bila sinyal bernilai 2 atau 3 maka directory tujuan akan diset dimana program C dijalankan.

Kemudian untuk menyusun daftar file file dalam directori terdapat fungsi listFilesRecursively yang akan menyusun file file tersebut sesuai dengan ekstensinya secara rekursif

    void listFilesRecursively(char *basePath){
        char path[256]={};
        struct dirent *dp;
        DIR *dir = opendir(basePath);

        if (!dir)
        return;

        while ((dp = readdir(dir)) != NULL)
        {
            if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0)
            {
                if (dp->d_type == DT_REG)
                {
                    strcpy(tanda[x], basePath);
                    strcat(tanda[x], dp->d_name);
                    x++;
                }
                else
                {
                    strcpy(path, basePath);
                    strcat(path, dp->d_name);
                    strcat(path, "/");
                    listFilesRecursively(path);
                }
            }
        }
        closedir(dir);
    }

- File-file di dalam direktori dan juga file-file di dalam subdirektori, semuanya disusun dalam sekumpulan array string yang dapat dikategorikan dengan fungsi persoalan pertama tadi. Untuk mengkategorikan file-file di dalam subdirektori, digunakan rekursi fungsi untuk menata daftar file di dalam direktori tersebut.
<br>
<b>Directori Hartakarun<b>
<br>
![hartakarun!](img/1/1.hartakarun.png)
<br>
<b>Unknown<b>
<br>
![Unknown!](img/1/1.unknown.png)
<br>
<b>Hidden<b>
<br>
![hidden!](img/1/1.hidden.png)

#### 3.c

Agar proses kategori bisa berjalan lebih cepat, setiap 1 file yang dikategorikan dioperasikan oleh 1 thread. Pada fungsi `main()` terdapat program sebagai berikut

    sinyal = 3;
	    int err;
	    listFilesRecursively(path);

	    for (i=0; i<x; i++){
		    err=pthread_create(&(thd[i]),NULL,&handler,(void *) tanda[i]);
		    
            if(err!=0) return 0;
	    }
	    for (i=0; i<x; i++) pthread_join(thd[i],NULL);

#### 3.d, e

Dengan menggunakan program client server, client dapat mengirimkan zip hasil pemrosesan yang dilakukan pada program soal.c ini ke dalam directori server dengan mengirimkan command send hartakarun.zip

    void* zip_file() {
        pid_t child_id;
        int status1;
            
        child_id = fork();
        if (child_id == 0) {
            printf("haloo");
            char *argv[] = {"zip", "-q", "-r", "hartakarun.zip", "/home/nauli/shift3/hartakarun", NULL};
            execv("/usr/bin/zip", argv);
        } else {
            while (wait(&status1) > 0);
        }
    }

- Pada bagian client ini pertama akan melakukan zip pada directori "/home/[user]/shift3/hartakarun/" dengan memangggil fungsi `zip_file()` pada fungsi main kemudian pada program client ini dapat mengirimkan command yang nantinya zip file tersebut akan kekirim kedalam direcotri servernya.

<br>
<b>client<b>
<br>
![client!](img/1/1.server.png)
<br>
<b>server<b>
<br>
![server!](img/1/1.client.png)
